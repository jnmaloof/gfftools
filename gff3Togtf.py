#!/usr/bin/env python3
#Short script to convert SoLyc gff3 to gtf
#Would need to be modified for other genomes/organisms
#Julin Maloof
#April 24, 2013

import fileinput, sys, re

if len(sys.argv)  == 1 :
	print("This script takes one or more gff3 files and converts them to gtf,")
	print("Use file names as arguments.")
	exit()

#create regular expression that can find lines for gene.  These will be thrown away
regGene = re.compile(r"^.+\t.+\tgene(\t[0-9.]+){3}\t[+-]\t\.\t.*ID=gene:")

#create regular expression to find transcript and geneID.  This is done on a per-line basis to deal with interspersed genes.
regGeneID = re.compile(r"^(.+\t){8}.*ID=.*:(?P<gene_id>Solyc[0-9]{2}g[0-9]{6}\.[0-9])")
regTranscriptID = re.compile(r"^(.+\t){8}.*ID=.+:(?P<transcript_id>Solyc[0-9]{2}g[0-9]{6}\.[0-9]\.[0-9])")

regEqual = re.compile(r"=([^;]+)")

for line in fileinput.input() :
	if line.startswith("#") : 
		#print(line,end="")
		continue
	#search for line containing gene id information
	geneMatch = regGene.match(line)
	if geneMatch :
		#print("gene match")
		continue
	geneIDMatch = regGeneID.match(line)
	if geneIDMatch :
		#print("geneID match")
		geneID = geneIDMatch.group("gene_id")	
	transcriptIDMatch = regTranscriptID.match(line)
	if transcriptIDMatch :
		transcriptID = transcriptIDMatch.group("transcript_id") 
		#print("transcriptID match")
	line = line.rstrip("\n")
	fields = line.split("\t")
	fields[8] = 'gene_id "' + geneID + '"; transcript_id "' + transcriptID + '"; ' + fields[8]
	fields[8] = regEqual.sub(r' "\1"',fields[8])
	fields[8] = fields[8].replace(";","; ")
	print("\t".join(fields))
