#Creating a gff file from cDNAs

Here I provide instructions and scripts for converting a fasta file of transcripts to a gff file suitable for display in a genome browser.

Step 1 is to map the transcripts to the genome.  I used [exonerate](http://www.ebi.ac.uk/~guy/exonerate/exonerate-2.2.0.tar.gz).  Another possible option is [genomeThreader](http://www.genomethreader.org/)

##exonerate settings

The default exonerate settings are too slow for whole genome work with thousands of transcripts.

###default settings take 11 minutes:
    exonerate --model est2genome  test.fa ../Brapa_sequence_v1.2.fa > test.out

### 1.5 minutes:
	time exonerate --model est2genome --bestn 2 --percent 90 \
	   --seedrepeat 10 test.fa ../Brapa_sequence_v1.2.fa > test2.out

### 7.2 seconds:
	time exonerate --model est2genome --bestn 2 --percent 90 \
	  --seedrepeat 50 test.fa ../Brapa_sequence_v1.2.fa > test2.out &
    
### 14 seconds
    time exonerate --model est2genome --bestn 1 --percent 95 --seedrepeat 10  --fsmmemory 3500 \
	   --showtargetgff T --showvulgar F  \
	   --query Bra0000001.fas \
	   --target ../../Brapa_sequence_v1.2.fa > exonerate.test.sr10 &


##shell loop for using multiple precessors:

(this should be converted into a script that takes arguments)

*note takes 12 hours on 4 processors for all B.rapa transcripts*

    i=1
    np=4 #number of cores
	  while [ $i -le $np ]
	  do
	  echo "$i"
	    time exonerate --model est2genome --bestn 1 --percent 95 --seedrepeat 10  --fsmmemory 3500 \
	    --showtargetgff T --showvulgar F  \
	    --querychunkid $i  --querychunktotal $np \
	    --query ../../Brassica_rapa_v1.2.cds \
	    --target ../../Brapa_sequence_v1.2.fa > exonerate.$i.out &
	    i=$(( i + 1 ))
    done

These settings capture most exons correctly but still miss very small 5' and 3' exons.

##python script for converting exonerate output to gff

see script in the files section of bit bucket.

*Important* UCSC doesn't display exon borders from the gff2 files that `exonerateTogff.py` produces.  Use `exonerateTogtf.py` instead

run using 
    exonerateTogtf.py exonerate.* > exonerate.all.gtf




